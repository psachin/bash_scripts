#!/usr/bin/env bash
# To find PCI Express passthrough device and class id on Azure VM.

# Usage: bash find_pci_express_device.sh
# Sample output:
# Device ID: {6e6a0941-902f-462a-9bf9-9ab73312e023}
# Class ID: {44c4f61d-4444-4400-9d52-802e27ede19f}

for dev in `ls -d /sys/bus/vmbus/devices/*`; do
 ls -d $dev/pci* &> /dev/null
 if [ $? -eq 0 ]; then
   echo "Device ID: `cat $dev/device_id`";
   echo "Class ID: `cat $dev/class_id`";
 fi;
done

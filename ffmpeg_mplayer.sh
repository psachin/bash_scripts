#!/usr/bin/env bash

# --- OpenSUSE 15.4/TumbleWeed specific ---
# for ffmpeg
sudo zypper install nasm  libmp3lame-devel libvpx-devel libXext-devel
# for Mplayer
sudo zypper install yasm-devel libvdpau-devel libpulse-devel
# ---

LAME_REPO="https://downloads.sourceforge.net/project/lame/lame/3.100/lame-3.100.tar.gz"
SOURCE_DIR="$HOME/source"
FF_SRC_DIR="$SOURCE_DIR/ffmpeg"
X264_SRC_DIR="$SOURCE_DIR/x264"
MPLAYER_SRC_DIR="$SOURCE_DIR/mplayer"
BUILD_DIR="$FF_SRC_DIR/build"
BIN_DIR="$HOME/bin"

if test -e $FF_SRC_DIR/.git ; then
    echo "Skip ffmpeg pull"
else
    git clone https://git.ffmpeg.org/ffmpeg.git $FF_SRC_DIR
fi

pushd $SOURCE_DIR
curl -O -L $LAME_REPO
tar xzvf lame-3.100.tar.gz
pushd lame-3.100/
make clean
./configure --prefix="$BUILD_DIR" \
            --bindir="$BIN_DIR" \
            --disable-shared \
            --enable-nasm
make -j5
make install
popd +0

if test -e $X264_SRC_DIR/.git; then
    echo "Skip x264 pull"
    pushd x264/
    make clean
    PKG_CONFIG_PATH="$BUILD_DIR/lib/pkgconfig" ./configure \
                   --prefix="$BUILD_DIR" \
                   --bindir="$BIN_DIR" \
                   --enable-static
    make -j10
    make install
else
    git clone --depth 1 https://code.videolan.org/videolan/x264
fi
popd +0

# --------------------------------------------------
# libx264: MP4 encoder
# libvpx: WEBM encoder
# libfreetype: Text overlay
# --------------------------------------------------
pushd $FF_SRC_DIR
make clean
PATH="$HOME/bin:$PATH" PKG_CONFIG_PATH="$BUILD_DIR/lib/pkgconfig" ./configure \
    --prefix="$BUILD_DIR" \
    --pkg-config-flags="--static" \
    --extra-cflags="-I$BUILD_DIR/include" \
    --extra-ldflags="-L$BUILD_DIR/lib" \
    --extra-libs=-lpthread \
    --extra-libs=-lm \
    --bindir="$BIN_DIR" \
    --enable-gpl \
    --enable-libmp3lame \
    --enable-libx264 \
    --enable-libvpx \
    --enable-nonfree \
    --enable-libfreetype \
    --enable-ffplay \
    --enable-filter=drawtext

make -j10
make install
popd +0

if test -e $MPLAYER_SRC_DIR/.svn; then
    echo "Skip Mplayer pull"
    pushd $MPLAYER_SRC_DIR
    make clean
    ./configure --prefix="$BUILD_DIR" --bindir=$BIN_DIR --enable-pulse --enable-vdpau --enable-gl
    make -j10
    make install
else
    svn checkout svn://svn.mplayerhq.hu/mplayer/trunk mplayer
fi
popd +0

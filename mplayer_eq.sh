#!/usr/bin/env bash
# -----------------------------------------------------------------------------
# To change equalizer preset in mplayer
# Derived from: http://advantage-bash.blogspot.in/2013/05/mplayer-presets.html
#
# 1. Setup
#    - Download this script and source it in ~/.bashrc
#      ~~~
#      source /path/to/mplayer_eq.sh
#      ~~~
# 2. Usage:
#    - Open a new terminal window/tab and execute `mplayer-eq`:
#      ~~~
#      mplayer-eq
#      ~~~
#
# For more info, refer to https://psachin.gitlab.io/mplayer_preset.html
# -----------------------------------------------------------------------------

EQ="af=equalizer="
CONFIG_PATH=$HOME/.mplayer/config

function mplayer-eq()
{
echo -e "1. Flat\n2. Classical\n3. Club\n4. Dance\n5. Full-bass\n6. Full-bass-and-treble\n7. Full-treble\n8. Headphones\n9. Large-hall\n10. Live\n11. Party\n12. Pop\n13. Reggae\n14. Rock\n15. Ska\n16. Soft\n17. Soft-rock\n18. Techno\n"

# Display present configuration if any.
if [ -f ${CONFIG_PATH} ];
then
    present_conf=$(grep -e '^${EQ}' -e '\#[a-zA-Z]' ~/.mplayer/config | awk 'BEGIN { FS = "="}; {print $3}')
    present_conf_name=$(grep "#" ${CONFIG_PATH} | awk '{print $NF}' | sed -e 's/#//g')
    if [ -z ${present_conf_name} ];
    then
	echo -e "Present configuration: Unknown"
    else
	echo -e "Present configuration:" ${present_conf_name}
    fi
else
    echo -e "No Audio equalizer preset is configured"
fi

read -p "your choice(or 'q' to quit): "
case $REPLY in
    1)sed -i -e "/${EQ}/c\\${EQ}0.0:0.0:0.0:0.0:0.0:0.0:0.0:0.0:0.0:0.0  #Flat" ${CONFIG_PATH}
      ;;
    2)sed -i -e "/${EQ}/c\\${EQ}0.0:0.0:0.0:0.0:0.0:-4.4:-4.4:-4.4:-5.8  #Classical" ${CONFIG_PATH}
      ;;
    3)sed -i -e "/${EQ}/c\\${EQ}0.0:0.0:4.8:3.3:3.3:3.3:1.9:0.0:0.0  #Club" ${CONFIG_PATH}
      ;;
    4)sed -i -e "/${EQ}/c\\${EQ}5.7:4.3:1.4:0.0:0.0:-3.4:-4.4:-4.3:0.0:0.0  #Dance" ${CONFIG_PATH}
      ;;
    5)sed -i -e "/${EQ}/c\\${EQ}-4.8:5.7:5.7:3.3:1.0:-2.4:-4.8:-6.3:-6.7:-6.7  #Full-bass" ${CONFIG_PATH}
      ;;
    6)sed -i -e "/${EQ}/c\\${EQ}4.3:3.3:0.0:-4.4:-2.9:1.0:4.8:6.7:7.2:7.2  #Full-bass-and-treble" ${CONFIG_PATH}
      ;;
    7)sed -i -e "/${EQ}/c\\${EQ}-5.8:-5.8:-5.8:-2.4:1.4:6.7:9.6:9.6:9.6:10.1  #Full-treble" ${CONFIG_PATH}
      ;;
    8)sed -i -e "/${EQ}/c\\${EQ}2.8:6.7:3.3:-2.0:-1.4:1.0:2.8:5.7:7.7:8.6  #Headphones" ${CONFIG_PATH}
      ;;
    9)sed -i -e "/${EQ}/c\\${EQ}6.2:6.2:3.3:3.3:0.0:-2.9:-2.9:-2.9:0.0:0.0  #Large-hall" ${CONFIG_PATH}
      ;;
    10)sed -i -e "/${EQ}/c\\${EQ}-2.9:0.0:2.4:3.3:3.3:3.3:2.4:1.4:1.4:1.4  #Live" ${CONFIG_PATH}
      ;;
    11)sed -i -e "/${EQ}/c\\${EQ}4.3:4.3:0.0:0.0:0.0:0.0:0.0:0.0:4.3:4.3  #Party" ${CONFIG_PATH}
      ;;
    12)sed -i -e "/${EQ}/c\\${EQ}-1.0:2.8:4.3:4.8:3.3:0.0:-1.4:-1.4:-1.0:-1.0  #Pop" ${CONFIG_PATH}
      ;;
    13)sed -i -e "/${EQ}/c\\${EQ}0.0:0.0:0.0:-3.4:0.0:3.8:3.8:0.0:0.0:0.0  #Reggae" ${CONFIG_PATH}
      ;;
    14)sed -i -e "/${EQ}/c\\${EQ}4.8:2.8:-3.4:-4.8:-2.0:2.4:5.3:6.7:6.7:6.7  #Rock" ${CONFIG_PATH}
      ;;
    15)sed -i -e "/${EQ}/c\\${EQ}-1.4:-2.9:-2.4:0.0:2.4:3.3:5.3:5.7:6.7:5.8  #Ska" ${CONFIG_PATH}
      ;;
    16)sed -i -e "/${EQ}/c\\${EQ}2.8:1.0:0.0:-1.4:0.0:2.4:4.8:5.7:6.7:7.2  #Soft" ${CONFIG_PATH}
      ;;
    17)sed -i -e "/${EQ}/c\\${EQ}2.4:2.4:1.4:0.0:-2.4:-3.4:-2.0:0.0:1.4:5.3  #Soft-rock" ${CONFIG_PATH}
      ;;
    18)sed -i -e "/${EQ}/c\\${EQ}4.8:3.3:0.0:-3.4:-2.9:0.0:4.8:5.7:5.8:5.3  #Techno" ${CONFIG_PATH}
      ;;
esac
}
